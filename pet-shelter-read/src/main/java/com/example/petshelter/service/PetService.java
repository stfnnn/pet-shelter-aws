package com.example.petshelter.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.petshelter.model.Pet;
import com.example.petshelter.repository.PetRepository;

@Service
@Transactional
public class PetService {

    private final PetRepository petRepository;

    public PetService(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    public Pet save(Pet pet) {
        return petRepository.save(pet);
    }

    @Transactional(readOnly = true)
    public List<Pet> findAll() {
        return petRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Pet> findOne(Long id) {
        return petRepository.findById(id);
    }

    public void delete(Long id) {
        petRepository.deleteById(id);
    }
}