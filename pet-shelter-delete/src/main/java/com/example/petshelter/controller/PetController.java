package com.example.petshelter.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.petshelter.model.Pet;
import com.example.petshelter.service.PetService;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/api")
public class PetController {
		
	private final PetService petService;

    public PetController(PetService petService) {
        this.petService = petService;
    }

//    @PostMapping("/pet")
//    public Pet createPet(@RequestBody Pet pet) {
//    	if (pet.getId() == null)
//    		return petService.save(pet);
//    	else
//    		return null;
//    }
    
//    @PutMapping("/pet")
//    public Pet updatePet(@RequestBody Pet pet) {
//    	if (pet.getId() != null)
//    		return petService.save(pet);
//    	else
//    		return null;
//    }
//    
//    @GetMapping("/pets")
//    public List<Pet> getPets() {
//    	return petService.findAll();
//    }
//    
//    @GetMapping("/pet")
//    public Optional<Pet> getPet(@RequestParam("id") Long id) {
//    	return petService.findOne(id);
//    }
    
    @DeleteMapping("/pet")
    public void deletePet(@RequestParam("id") Long id) {
    	petService.delete(id);
    }
}
