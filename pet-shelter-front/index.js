$( document ).ready(function() {
	 $.ajax({
         type: "GET",
         url: "http://localhost:9001/pet-shelter-read/api/pets",
         success: function (data) {
        	 table = "";
             data.forEach(p => {
                 table += "<tr>";
                 table += "<td>" + p.id + "</td>";
                 table += "<td>" + p.type + "</td>";
                 table += "<td>" + p.breed + "</td>";
                 table += "<td>" + p.name + "</td>";
                 table += "<td>" + p.birthYear + "</td>";
                 table += "<td>" + p.receiptDate + "</td>";
                 table += "<td>" + p.color + "</td>";
                 table += "<td>" + "<button class='edit' onClick='editPet(" + JSON.stringify(p) + ")'>Edit</button><button class='delete' onClick='deletePet(" + p.id + ")'>Delete</button>" + "</td>";
                 table += "</tr>";
             });
             document.getElementById("tablebody").innerHTML = table;
         }
     });
});

function deletePet(id) {
    $.ajax({
        type: "DELETE",
        url: "http://localhost:9001/pet-shelter-delete/api/pet?id=" + id,
        success: function (data) {
        	location.reload();
        }
    });
}

function editPet(p) {
	table = document.getElementById("tablebody").innerHTML;
    table += "<tr>";
    table += "<td><input type='text' value='" + p.id + "' id='id' style='width:80%' disabled></td>";
    table += "<td><input type='text' value='" + p.type + "' id='type'></td>";
    table += "<td><input type='text' value='" + p.breed + "' id='breed'></td>";
    table += "<td><input type='text' value='" + p.name + "' id='name'></td>";
    table += "<td><input type='number' value='" + p.birthYear + "' id='birthYear'></td>";
    table += "<td><input type='date' value='" + p.receiptDate + "' id='receiptDate'></td>";
    table += "<td><input type='text' value='" + p.color + "' id='color'></td>";
    table += "<td>" + "<button class='accept' onclick='acceptUpdate()'>Accept</button><button class='cancel' onclick='cancel()'>Cancel</button></td>";
    table += "</tr>";
    
document.getElementById("tablebody").innerHTML = table;
}

function addNew() {
	table = document.getElementById("tablebody").innerHTML;
        table += "<tr>";
        table += "<td></td>";
        table += "<td><input type='text' id='type'></td>";
        table += "<td><input type='text' id='breed'></td>";
        table += "<td><input type='text' id='name'></td>";
        table += "<td><input type='number' id='birthYear'></td>";
        table += "<td><input type='date' id='receiptDate'></td>";
        table += "<td><input type='text' id='color'></td>";
        table += "<td>" + "<button class='accept' onclick='accept()'>Accept</button><button class='cances' onclick='cancel()'>Cancel</button></td>";
        table += "</tr>";
        
    document.getElementById("tablebody").innerHTML = table;
}

function accept() {
	var pet = new Object();
	pet.type = $( "#type" ).val();
	pet.breed = $( "#breed" ).val();
	pet.name = $( "#name" ).val();
	pet.birthYear = $( "#birthYear" ).val();
	pet.receiptDate = $( "#receiptDate" ).val();
	pet.color = $( "#color" ).val();
	
	$.ajax({
	    type: "POST",
	    url: "http://localhost:9001/pet-shelter-update/api/pet",
	    headers: { 
	        'Accept': 'application/json',
	        'Content-Type': 'application/json' 
	    },
	    data: JSON.stringify(pet),
	    success: function (data) {
	        location.reload();
	    }
	});		
}

function cancel() {
	location.reload();
}

function acceptUpdate() {
	var pet = new Object();
	pet.id = $( "#id" ).val();
	pet.type = $( "#type" ).val();
	pet.breed = $( "#breed" ).val();
	pet.name = $( "#name" ).val();
	pet.birthYear = $( "#birthYear" ).val();
	pet.receiptDate = $( "#receiptDate" ).val();
	pet.color = $( "#color" ).val();
	
	$.ajax({
	    type: "PUT",
	    url: "http://localhost:9001/pet-shelter-update/api/pet",
	    headers: { 
	        'Accept': 'application/json',
	        'Content-Type': 'application/json' 
	    },
	    data: JSON.stringify(pet),
	    success: function (data) {
	        location.reload();
	    }
	});		
}
